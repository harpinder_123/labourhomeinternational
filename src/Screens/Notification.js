import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  ImageBackgroundBase,
  ImageBackground,
  FlatList,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, HeaderDark, MainView} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Notification = ({navigation}) => {
  const [data, setData] = useState([
    {
      id: '1',
      title: 'Just Now',
      source: require('../images/calender.png'),
    },
    {
      id: '2',
      source: require('../images/user2.png'),
    },
    {
      id: '3',
      title: 'Just Now',
      source: require('../images/calender.png'),
    },
    {
      id: '4',
      source: require('../images/user2.png'),
    },
  ]);
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarLight />
      <HeaderDark onPress={() => navigation.goBack()} title={'Notification'} />
      <View>
        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <View style={{flexDirection: 'row'}}>
              <Image style={styles.image} source={item.source} />
              <View>
                <Text style={styles.text}>
                  You have video call interview with at{`\n`}
                  <Text style={{fontWeight: '700'}}>08:00 PM today.</Text>
                </Text>
                <Text style={{marginLeft: 30, color: '#9393AA', marginTop: 5}}>
                  {item.title}
                </Text>
              </View>
            </View>
          )}
        />
      </View>
    </View>
  );
};

export default Notification;

const styles = StyleSheet.create({
  subBox: {
    padding: 10,
    marginHorizontal: 15,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 5,
  },
  image: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginTop: 30,
    marginLeft: 30,
  },
  text: {
    fontFamily: 'Muli-Regular',
    fontSize: 15,
    fontWeight: '500',
    color: '#1E1F20',
    marginLeft: 30,
    marginTop: 30,
    lineHeight: 24,
  },
});
