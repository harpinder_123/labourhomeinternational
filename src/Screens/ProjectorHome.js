import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
  ImageBackground,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header2} from '../Custom/CustomView';

import Banner from '../home/Banner';
const {height} = Dimensions.get('window');

const ProjectorHome = ({navigation}) => {
  const [data, setData] = useState([
    {
      keys: 1,
      title: 'My\nProfile',
      source: require('../images/Reruiting-agent-slice/project-exporter/background.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/user.png'),
    },
    {
      keys: 2,
      title: 'Posted\nJobs',
      source: require('../images/Reruiting-agent-slice/project-exporter/background1.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/post.png'),
    },
    {
      keys: 3,
      title: 'Candidate\nApplied',
      source: require('../images/Reruiting-agent-slice/project-exporter/background3.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/candidate.png'),
    },
    {
      keys: 4,
      title: 'Schedule\nInterview',
      source: require('../images/Reruiting-agent-slice/project-exporter/background4.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/schedule.png'),
    },
    {
      keys: 5,
      title: 'Help &\nSupport',
      source: require('../images/Reruiting-agent-slice/project-exporter/background5.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/support.png'),
    },
    {
      keys: 6,
      title: 'Faq ask\nQuestion',
      source: require('../images/Reruiting-agent-slice/project-exporter/background6.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/faq.png'),
    },
    {
      keys: 7,
      title: 'App\nSettings',
      source: require('../images/Reruiting-agent-slice/project-exporter/background7.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/setting.png'),
    },
    {
      keys: 8,
      title: 'App\nLogout',
      source: require('../images/Reruiting-agent-slice/project-exporter/background8.png'),
      image: require('../images/Reruiting-agent-slice/project-exporter/logout.png'),
    },
  ]);

  const onPressCategory = (item, index) => {
    const {keys} = item;
    switch (keys) {
      case 1:
        navigation.navigate('Profile1');
        break;

      case 2:
        navigation.navigate('PostedJobs');
        break;

      case 3:
        navigation.navigate('Candidate');
        break;

      case 4:
        navigation.navigate('ScheduleInterview');
        break;

      case 5:
        navigation.navigate('Support');
        break;

      case 6:
        navigation.navigate('Faq');
        break;

      case 7:
        navigation.navigate('Setting');
        break;

      case 8:
        navigation.navigate('Login');
        break;
      default:
    }
  };
  return (
    <View style={{backgroundColor: '#f8f8f8', flex: 1}}>
      <StatusBarLight />
      <Header2 onPress={() => navigation.goBack()} />
      <ScrollView>
        <Banner />
        <View style={{marginTop: 40, marginHorizontal: 20}}>
          <FlatList
            numColumns={3}
            keyExtractor={item => item.id}
            data={data}
            renderItem={({item, index}) => (
              <TouchableOpacity onPress={() => onPressCategory(item, index)}>
                <ImageBackground
                  source={item.source}
                  style={{
                    width: 100,
                    height: 92,
                    resizeMode: 'contain',
                    margin: 10,
                  }}>
                  <Image
                    style={{
                      width: 23,
                      height: 23,
                      resizeMode: 'contain',
                      marginTop: 10,
                      marginLeft: 10,
                    }}
                    source={item.image}
                  />
                  <Text style={styles.text}>{item.title}</Text>
                </ImageBackground>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default ProjectorHome;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Muli-SemiBold',
    fontSize: 14,
    fontWeight: '700',
    color: '#010101',
    marginLeft: 10,
    marginTop: 10,
  },
});
