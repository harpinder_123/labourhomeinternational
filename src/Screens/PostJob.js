import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
  TextInput,
  Modal,
  Option,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {Header, HeaderDark} from '../Custom/CustomView';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import CountryPicker from 'react-native-country-picker-modal';
import {BottomView, ButtonStyle, StartButton} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const PostJob = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [country, setCountry] = useState([{value: 'India'}]);
  const [selectJob, setSelectJob] = useState([{value: 'Construction'}]);
  const [category, setCategory] = useState([{value: 'Material'}]);
  const [countryCode, setCountryCode] = useState('IN');
  const [callingCode, setCallingCode] = useState('91');

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarLight />
      <HeaderDark onPress={() => navigation.goBack()} title={'Post Job'} />
      <ScrollView>
        <TextLabel title={'Employer Name'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Employer Name'}
        />
        <TextLabel title={'Select Country'} />
        <View
          style={{
            borderRadius: 6,
            borderWidth: 1,
            padding: 10,
            backgroundColor: '#ffffff',
            borderColor: 'lightgrey',
            marginTop: 10,
            flexDirection: 'row',
            width: '85%',
            justifyContent: 'space-between',
            alignContent: 'center',
            alignSelf: 'center',
            // marginBottom: 20,
          }}>
          <CountryPicker
            countryCode={countryCode}
            withFilter
            withFlag
            withCountryNameButton
            withAlphaFilter={false}
            withCallingCode
            onSelect={country => {
              console.log('country', country);
              const {cca2, callingCode} = country;
              setCountryCode(cca2);
              setCallingCode(callingCode[0]);
            }}
            containerButtonStyle={{alignItems: 'center'}}
          />
          <Image
            style={{
              width: 25,
              height: 12,
              resizeMode: 'contain',
              marginTop: 10,
            }}
            source={require('../images/down-arrow.png')}
          />
        </View>
        <TextLabel title={'Job Location'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Job Location'}
        />
        <TextLabel title={'Work Experience'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Work Experience'}
          keyboardType={'number-pad'}
        />
        <TextLabel title={'Qualification'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Qualification'}
        />
        <TextLabel title={'Labour Required'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Labour Required'}
        />
        <TextLabel title={'Select Category'} />
        <View style={{marginTop: 10}}>
          <Dropdown
            value="Construction"
            style={styles.drops}
            itemColor={'rgba(0, 0, 0, .54)'}
            underlineColor="transparent"
            // label={'Select User Type'}
            // icon="cheveron-down"
            iconColor="rgba(0, 0, 0, 1)"
            icon={require('../images/down-arrow.png')}
            dropdownOffset={{top: 32, left: 0}}
            dropdownMargins={{min: 8, max: 16}}
            pickerStyle={{width: '84%', left: '8%'}}
            dropdownPosition={-1.9}
            shadeOpacity={0.12}
            rippleOpacity={0.4}
            baseColor={'white'}
            data={selectJob}
            onChangeText={(value, index, data) => {
              onChangeDropdownUserType(value, index, data);
            }}
          />
        </View>
        <TextLabel title={'Select Sub Category'} />
        <View style={{marginTop: 10}}>
          <Dropdown
            value="Material"
            style={styles.drops}
            itemColor={'rgba(0, 0, 0, .54)'}
            underlineColor="transparent"
            // label={'Select User Type'}
            // icon="cheveron-down"
            iconColor="rgba(0, 0, 0, 1)"
            icon={require('../images/down-arrow.png')}
            dropdownOffset={{top: 32, left: 0}}
            dropdownMargins={{min: 8, max: 16}}
            pickerStyle={{width: '84%', left: '8%'}}
            dropdownPosition={-1.9}
            shadeOpacity={0.12}
            rippleOpacity={0.4}
            baseColor={'white'}
            data={category}
            onChangeText={(value, index, data) => {
              onChangeDropdownUserType(value, index, data);
            }}
          />
        </View>
        <TextLabel title={'Job Description'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Job Description'}
        />
        <TextLabel title={'Contract Period'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Contract Period'}
        />
        <TextLabel title={'Accommodation'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Accommodation'}
        />
        <TextLabel title={'Food'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Food'}
        />
        <TextLabel title={'Working Hours'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Working Hours'}
        />
        <TextLabel title={'Transportation'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Transportation'}
        />
        <TextLabel title={'Medical & Insurance'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Medical & Insurance'}
        />
        <TextLabel title={'Annual Leave'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Annual Leave'}
        />
        <TextLabel title={'Air Ticket'} />
        <TextInput
          // value={state.name}
          //   onChangeText={name => setState({...state, name})}
          style={styles.textInput}
          placeholder={'Air Ticket'}
        />
        <View style={{width: '90%', alignSelf: 'center', marginTop: 30}}>
          <ButtonStyle title={'SUBMIT'} onPress={() => setModalOpen(true)} />
        </View>
        <BottomView />
        <Modal
          visible={modalOpen}
          transparent={true}
          onRequestClose={() => setModalOpen(false)}>
          <View style={styles.modal_View}>
            <View activeOpacity={0.8} style={styles.mdtop}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 20,
                }}
                source={require('../images/tick.png')}
              />
              <Text style={styles.text}>
                Job Post{`\n`}
                Successfully
              </Text>
              <View style={{width: '35%', alignSelf: 'center', marginTop: 20}}>
                <StartButton
                  title={'OK'}
                  onPress={() => {
                    navigation.navigate('ProjectorHome');
                  }}
                />
              </View>
              <BottomView />
            </View>
          </View>
        </Modal>
      </ScrollView>
    </View>
  );
};

export default PostJob;
const TextLabel = ({title}) => <Text style={styles.textLabel}>{title}</Text>;
const styles = StyleSheet.create({
  textInput: {
    borderRadius: 6,
    borderWidth: 1,
    padding: 10,
    paddingHorizontal: 15,
    marginHorizontal: 30,
    marginTop: 10,
    marginBottom: 0,
    fontSize: 16,
    fontFamily: 'Muli-SemiBold',
    fontWeight: '600',
    backgroundColor: '#fff',
    borderColor: 'lightgrey',
  },
  textLabel: {
    fontFamily: 'Nunito',
    fontWeight: '900',
    fontSize: 14,
    color: '#8F9BB3',
    marginHorizontal: 30,
    marginTop: 30,
    marginBottom: 0,
  },
  drops: {
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 6,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: 'lightgrey',
    marginHorizontal: 30,
    // elevation: 2,
  },
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  text: {
    fontFamily: 'Muli-Bold',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#1E1F20',
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 25,
  },
});
