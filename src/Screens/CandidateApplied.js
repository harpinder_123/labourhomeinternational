import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
  Modal,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import AppHeader from '../Custom/CustomAppHeader';
import {
  BottomView,
  EndButton,
  ButtonStyle,
  StartButton,
} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {useNavigation} from '@react-navigation/native';
import CheckBox from '@react-native-community/checkbox';
const {height} = Dimensions.get('window');

const APPLY = () => {
  const navigation = useNavigation();
  const [state, setState] = useState({
    term: false,
  });
  const [data, setData] = useState([
    {
      id: '1',
      title: 'Gourav Sharma',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user2.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
    {
      id: '2',
      title: 'Deepak Kumar',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user1.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
  ]);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <View style={styles.box}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.image}
              source={require('../images/Reruiting-agent-slice/user1.png')}
            />
            <Text style={styles.name}>Deepak Kumar</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.userImage}
              source={require('../images/Reruiting-agent-slice/portfolio.png')}
            />
            <Text style={styles.userText}>28 * 6 yrs experience</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.user2Image}
              source={require('../images/Reruiting-agent-slice/language.png')}
            />
            <Text style={styles.user2Text}>English, Hindi</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.user2Image}
              source={require('../images/Reruiting-agent-slice/email.png')}
            />
            <Text style={styles.user3Text}>deepak@gmail.com</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.user2Image}
              source={require('../images/Reruiting-agent-slice/iphone.png')}
            />
            <Text style={styles.user3Text}>+91 9599499793</Text>
          </View>
          <View style={{marginLeft: 55, marginTop: 10, flexDirection: 'row'}}>
            <CheckBox
              // style={{width: 20, height: 20}}
              disabled={false}
              value={state.term}
              onValueChange={term => setState({...state, term})}
              tintColors={{true: '#00C327', false: 'grey'}}
            />
            <Text style={styles.interviewText}>Interview Done</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: 10,
              marginBottom: 5,
            }}>
            <View style={{width: '35%', marginLeft: 25}}>
              <EndButton
                title={'Approve'}
                height={30}
                fontSize={12}
                bgColor={'#00C327'}
                txtcolor={'#fff'}
                onPress={() => {}}
              />
            </View>
            <View style={{width: '35%'}}>
              <EndButton
                title={'Reject'}
                height={30}
                fontSize={12}
                bgColor={'#DA274D'}
                txtcolor={'#fff'}
              />
            </View>
          </View>
        </View>

        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <View style={styles.box}>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.image} source={item.source} />
                <Text style={styles.name}>{item.title}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.userImage}
                  source={require('../images/Reruiting-agent-slice/portfolio.png')}
                />
                <Text style={styles.userText}>{item.year}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/language.png')}
                />
                <Text style={styles.user2Text}>{item.language}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/email.png')}
                />
                <Text style={styles.user3Text}>{item.email}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/iphone.png')}
                />
                <Text style={styles.user3Text}>{item.phone}</Text>
              </View>
              <View style={{width: '50%', marginLeft: 55, marginTop: 10}}>
                <EndButton
                  title={'Schedule Interview'}
                  height={30}
                  fontSize={12}
                  bgColor={'#2574FF'}
                  txtcolor={'#fff'}
                  onPress={() => {
                    navigation.navigate('Subscription');
                  }}
                />
              </View>
            </View>
          )}
        />
      </ScrollView>
    </View>
  );
};
const APPROVE = () => {
  const navigation = useNavigation();
  const [data, setData] = useState([
    {
      id: '1',
      title: 'Deepak Kumar',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user1.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
    {
      id: '2',
      title: 'Gourav Sharma',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user2.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
    {
      id: '3',
      title: 'Deepak Kumar',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user1.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
  ]);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <View style={styles.box}>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.image} source={item.source} />
                <Text style={styles.name}>{item.title}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.userImage}
                  source={require('../images/Reruiting-agent-slice/portfolio.png')}
                />
                <Text style={styles.userText}>{item.year}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/language.png')}
                />
                <Text style={styles.user2Text}>{item.language}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/email.png')}
                />
                <Text style={styles.user3Text}>{item.email}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/iphone.png')}
                />
                <Text style={styles.user3Text}>{item.phone}</Text>
              </View>
              <View style={{width: '45%', marginLeft: 55, marginTop: 10}}>
                <EndButton
                  title={'View Details'}
                  height={30}
                  fontSize={12}
                  bgColor={'#2574FF'}
                  txtcolor={'#fff'}
                  onPress={() => {
                    navigation.navigate('UserDetail');
                  }}
                />
              </View>
            </View>
          )}
        />
      </ScrollView>
    </View>
  );
};
const REJECT = () => {
  const navigation = useNavigation();
  const [data, setData] = useState([
    {
      id: '1',
      title: 'Deepak Kumar',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user1.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
    {
      id: '2',
      title: 'Gourav Sharma',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user2.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
    {
      id: '3',
      title: 'Deepak Kumar',
      language: 'English, Hindi',
      source: require('../images/Reruiting-agent-slice/user1.png'),
      year: '28 * 6 yrs experience',
      email: 'deepak@gmail.com',
      phone: '+91 9599499793',
    },
  ]);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <View style={styles.box}>
              <View style={{flexDirection: 'row'}}>
                <Image style={styles.image} source={item.source} />
                <Text style={styles.name}>{item.title}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.userImage}
                  source={require('../images/Reruiting-agent-slice/portfolio.png')}
                />
                <Text style={styles.userText}>{item.year}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/language.png')}
                />
                <Text style={styles.user2Text}>{item.language}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/email.png')}
                />
                <Text style={styles.user3Text}>{item.email}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.user2Image}
                  source={require('../images/Reruiting-agent-slice/iphone.png')}
                />
                <Text style={styles.user3Text}>{item.phone}</Text>
              </View>
              <View style={{width: '30%', marginLeft: 55, marginTop: 10}}>
                <EndButton
                  title={'Rejected'}
                  height={30}
                  fontSize={12}
                  bgColor={'#DA274D'}
                  txtcolor={'#fff'}
                  onPress={() => {}}
                />
              </View>
            </View>
          )}
        />
      </ScrollView>
    </View>
  );
};

const renderScene = SceneMap({
  first: APPLY,
  second: APPROVE,
  third: REJECT,
});

const CandidateApplied = ({navigation}) => {
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'APPLY'},
    {key: 'second', title: 'APPROVE'},
    {key: 'third', title: 'REJECT'},
  ]);
  return (
    <View style={{backgroundColor: '#f8f8f8', flex: 1}}>
      <StatusBarLight />
      <AppHeader
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../images/back.png')}
        title={'Candidates Applied'}
        shareOnClick={() => {}}
        share={require('../images/Reruiting-agent-slice/support.png')}
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            scrollEnabled={false}
            activeColor={'#2574FF'}
            inactiveColor={'#000000'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default CandidateApplied;

const styles = StyleSheet.create({
  style: {backgroundColor: 'white', elevation: 5},
  labelStyle: {
    fontSize: 16,
    fontFamily: 'Muli',
    fontWeight: 'bold',
    color: '#ACB1C0',
  },
  indicatorStyle: {
    backgroundColor: '#2574FF',
    height: 3,
  },
  box: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 14,
    elevation: 5,
    marginTop: 40,
    marginBottom: 10,
  },
  image: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
    marginLeft: -30,
    marginTop: -30,
  },
  name: {
    fontSize: 16,
    fontFamily: 'Muli-Bold',
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 15,
  },
  userImage: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginLeft: 65,
    marginTop: -15,
  },
  userText: {
    fontFamily: 'Muli-Bold',
    fontSize: 14,
    fontWeight: '400',
    color: '#1E1F20',
    marginTop: -19,
    marginLeft: 10,
  },
  user2Image: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginLeft: 65,
    marginTop: 10,
  },
  user2Text: {
    fontFamily: 'Muli-Bold',
    fontSize: 14,
    fontWeight: '400',
    color: '#1E1F20',
    marginTop: 7,
    marginLeft: 10,
  },
  user3Text: {
    fontFamily: 'Muli-Bold',
    fontSize: 14,
    fontWeight: '400',
    color: '#1E1F20',
    marginTop: 7,
    marginLeft: 10,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0.5, height: 0.5},
    textShadowRadius: 10,
  },
  interviewText: {
    fontFamily: 'Muli-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#1E1F20',
    marginTop: 5,
  },
});
