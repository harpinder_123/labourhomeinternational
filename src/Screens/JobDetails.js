import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {HeaderDark} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Timeline from 'react-native-timeline-flatlist';
import {BottomView, ButtonStyle} from '../Custom/CustomView';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');

const JOBDETAILS = () => {
  const [data, setData] = useState([
    {
      title: '3-6 Years',
      source: require('../images/portfolio.png'),
    },
    {
      title: '100 Opening',
      source: require('../images/filter-manager.png'),
    },
    {
      title: 'Washington, United States',
      source: require('../images/location1.png'),
    },
    {
      title: 'Not disclosed',
      source: require('../images/book.png'),
    },
    {
      title: 'Hard Hat, Mason, Brick Layer, Maker',
      source: require('../images/pen-tool.png'),
    },
    {
      title: 'Contract period 2 years',
      source: require('../images/book.png'),
    },
    {
      title: ' Accommodation provided by company',
      source: require('../images/home.png'),
    },
    {
      title: 'Food provided by company',
      source: require('../images/salad.png'),
    },
    {
      title: '8 hours per day',
      source: require('../images/clock.png'),
    },
    {
      title: 'Transportation provided by company',
      source: require('../images/bus.png'),
    },
    {
      title: 'Medical & Insurance provided company',
      source: require('../images/first-aid-kit.png'),
    },
    {
      title: 'Air ticket provided by company',
      source: require('../images/airplane.png'),
    },
  ]);
  return (
    <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
      <ScrollView>
        <View style={styles.subBox}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{
                width: 50,
                height: 50,
                marginLeft: 10,
                marginTop: 15,
              }}
              source={require('../images/images.png')}
            />
            <View>
              <Text style={styles.inText}>Ram Construction</Text>
              <Text style={styles.insubText}>Washington, United States</Text>
            </View>
            <Image
              style={{
                width: 16,
                height: 16,
                marginLeft: 'auto',
                marginHorizontal: 15,
                marginTop: 15,
              }}
              source={require('../images/star.png')}
            />
          </View>
          <Text style={styles.middleText}>
            Hard Hat, Mason, Brick Layer, Maker…{' '}
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.redText}>3-6 Years</Text>
            <Text style={styles.redText}>Posted on Sep 29</Text>
          </View>
        </View>
        <View style={styles.subBox}>
          <FlatList
            numColumns={1}
            data={data}
            renderItem={({item, index}) => (
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={{
                    width: 17,
                    height: 17,
                    resizeMode: 'contain',
                    marginTop: 15,
                    marginLeft: 5,
                  }}
                  source={item.source}
                />
                <Text style={styles.boxText}>{item.title}</Text>
              </View>
            )}
          />
        </View>
        <Text style={styles.job}>Job Description</Text>
        <View style={styles.subBox}>
          <Text style={styles.aboutHeading}>What you’ll do</Text>
          <Text style={styles.about}>
            This opening is for 0-6months experience candidates. Should have
            worked on Angular JS/Javascript, MeanStack. Work on agile tools with
            US Clients/Projects. Looking for self motivated individual with zeal
            to learn and grow.
          </Text>
        </View>
        <View style={styles.subBox}>
          <Text style={styles.text}>Industry type</Text>
          <Text style={styles.subText}>Construction Worker</Text>
          <Text style={styles.text}>Functional Area</Text>
          <Text style={styles.subText}>Other</Text>
          <Text style={styles.text}>Employment Type</Text>
          <Text style={styles.subText}>Full Time, Permanent</Text>
          <Text style={styles.text}>Education</Text>
          <Text style={styles.subText}>10th in any Specialization</Text>
          <Text style={styles.subText}>12th in any Specialization</Text>
        </View>
      </ScrollView>
    </View>
  );
};

const JOBSTATUS = () => {
  const navigation = useNavigation();
  const data = [
    {
      title: (
        <Text>
          {' '}
          Job Applied <Text style={{color: '#8F9BB3'}}>by Thu, 05 Oct</Text>
        </Text>
      ),
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
    },
    {
      title: (
        <Text>
          Approved <Text style={{color: '#8F9BB3'}}>today</Text>
        </Text>
      ),
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
    },
    {
      title: (
        <Text>
          Document Submit <Text style={{color: '#8F9BB3'}}>by Sat, 09 Oct</Text>
        </Text>
      ),
      icon: require('../images/tick1.png'),
    },
    {
      title: (
        <Text>
          Interview Call <Text style={{color: '#8F9BB3'}}>on Fri, 15 Oct</Text>
        </Text>
      ),
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
      description: '(Location: C-9/21 Rohini Sector-7 2nd Floor)',
    },
    {
      title: (
        <Text>
          Interview Done <Text style={{color: '#8F9BB3'}}>on Mon, 18 Oct</Text>
        </Text>
      ),
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
    },
    {
      title: 'Upload Skill Certificate',
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
      imageUrl: require('../images/Reruiting-agent-slice/skill.png'),
    },
    {
      title: 'Status Approved',
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
    },
    {
      title: 'Certificate Verified',
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick.png')}
        />
      ),
    },
    {
      title: (
        <Text style={{color: '#8F9BB3'}}>
          Make Payment (min amount ₹5000/-)
        </Text>
      ),
      icon: (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={require('../images/tick2.png')}
        />
      ),
    },
  ];
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <View style={styles.sub2Box}>
          <Text style={styles.viewJob}>Job Status</Text>
          <View style={styles.Line} />
          <Timeline
            data={data}
            circleColor={'#00AA83'}
            lineColor={'#00AA83'}
            listViewStyle={{marginLeft: -25}}
            innerCircle={'icon'}
            iconStyle={{
              height: 9,
              width: 9,
              resizeMode: 'contain',
            }}
            titleStyle={{
              color: '#00AA83',
              fontSize: 14,
              marginBottom: 30,
              marginTop: -13,
            }}
            descriptionStyle={{
              color: '#8F9BB3',
              fontSize: 12,
              marginTop: -10,
              marginBottom: 10,
            }}
          />
          <View style={{width: '50%', marginLeft: 30}}>
            <ButtonStyle
              title={'PAYMENT NOW'}
              height={40}
              onPress={() => {
                navigation.navigate('Payment');
              }}
            />
          </View>
          <BottomView />
        </View>
      </ScrollView>
    </View>
  );
};

const renderScene = SceneMap({
  first: JOBDETAILS,
  second: JOBSTATUS,
});

const JobDetails = ({navigation}) => {
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'JOB DETAIL'},
    {key: 'second', title: 'JOB STATUS'},
  ]);
  return (
    <View style={{backgroundColor: '#f8f8f8', flex: 1}}>
      <StatusBarLight />
      <HeaderDark onPress={() => navigation.goBack()} title={'Job Status'} />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            scrollEnabled={false}
            activeColor={'#2574FF'}
            inactiveColor={'#000000'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default JobDetails;

const styles = StyleSheet.create({
  image: {
    marginTop: height / 3,
    width: 219,
    height: 232,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  style: {backgroundColor: 'white', elevation: 5},
  labelStyle: {
    fontSize: 16,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#ACB1C0',
  },
  indicatorStyle: {
    backgroundColor: '#2574FF',
    height: 3,
  },
  subBox: {
    padding: 10,
    marginHorizontal: 15,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    marginTop: 20,
    // marginLeft: 25,
    marginBottom: 5,
  },
  sub2Box: {
    width: 335,
    height: 575,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    marginTop: 20,
    // marginLeft: 25,
    marginBottom: 5,
    alignSelf: 'center',
  },
  inText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginLeft: 15,
    marginTop: 15,
  },
  insubText: {
    fontFamily: 'Muli',
    fontSize: 12,
    fontWeight: '600',
    color: '#8F9BB3',
    marginLeft: 15,
    marginTop: 5,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#f5f5f5',
    // marginHorizontal: 20,
    marginTop: 10,
  },
  redText: {
    fontFamily: 'Muli',
    fontSize: 12,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 15,
    marginTop: 15,
    marginBottom: 10,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#f5f5f5',
    // marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 20,
  },
  middleText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '700',
    color: '#8F9BB3',
    marginLeft: 15,
    marginTop: 15,
  },
  boxText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 15,
    marginTop: 13,
  },
  job: {
    fontFamily: 'Muli',
    fontSize: 18,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 20,
    marginTop: 20,
  },
  viewJob: {
    fontFamily: 'Muli',
    fontSize: 18,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 30,
    marginTop: 20,
  },
  aboutHeading: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 5,
  },
  about: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '600',
    color: '#1E1F20',
    marginLeft: 5,
    marginTop: 5,
    lineHeight: 22,
    textAlign: 'justify',
    marginHorizontal: 20,
  },
  text: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '600',
    color: '#8F9BB3',
    marginLeft: 5,
    marginTop: 5,
  },
  subText: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 5,
    marginTop: 5,
  },
});
