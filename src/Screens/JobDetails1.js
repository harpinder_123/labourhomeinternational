import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
  Modal,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import AppHeader from '../Custom/CustomAppHeader';
import {BottomView, EndButton, ButtonStyle} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');

const JOBDETAILS = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const navigation = useNavigation();
  const [data, setData] = useState([
    {
      title: '3-6 Years',
      source: require('../images/portfolio.png'),
    },
    {
      title: '100 Opening',
      source: require('../images/filter-manager.png'),
    },
    {
      title: 'Washington, United States',
      source: require('../images/location1.png'),
    },
    {
      title: 'Not disclosed',
      source: require('../images/book.png'),
    },
    {
      title: 'Hard Hat, Mason, Brick Layer, Maker',
      source: require('../images/pen-tool.png'),
    },
    {
      title: 'Contract period 2 years',
      source: require('../images/book.png'),
    },
    {
      title: ' Accommodation provided by company',
      source: require('../images/home.png'),
    },
    {
      title: 'Food provided by company',
      source: require('../images/salad.png'),
    },
    {
      title: '8 hours per day',
      source: require('../images/clock.png'),
    },
    {
      title: 'Transportation provided by company',
      source: require('../images/bus.png'),
    },
    {
      title: 'Medical & Insurance provided company',
      source: require('../images/first-aid-kit.png'),
    },
    {
      title: 'Air ticket provided by company',
      source: require('../images/airplane.png'),
    },
  ]);
  return (
    <View style={{flex: 1, backgroundColor: '#f8f8f8'}}>
      <ScrollView>
        <View style={styles.subBox}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{
                width: 50,
                height: 50,
                marginLeft: 5,
                marginTop: 15,
              }}
              source={require('../images/images.png')}
            />
            <View>
              <Text style={styles.inText}>Ram Construction</Text>
              <Text style={styles.insubText}>Washington, United States</Text>
            </View>
            <Image
              style={{
                width: 16,
                height: 16,
                marginLeft: 'auto',
                marginHorizontal: 15,
                marginTop: 15,
              }}
              source={require('../images/star.png')}
            />
          </View>
          <Text style={styles.middleText}>
            Hard Hat, Mason, Brick Layer, Maker…{' '}
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.redText}>3-6 Years</Text>
            <Text style={styles.redText}>Posted on Sep 29</Text>
          </View>
        </View>
        <View style={styles.subBox}>
          <FlatList
            numColumns={1}
            data={data}
            renderItem={({item, index}) => (
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={{
                    width: 17,
                    height: 17,
                    resizeMode: 'contain',
                    marginTop: 15,
                    marginLeft: 5,
                  }}
                  source={item.source}
                />
                <Text style={styles.boxText}>{item.title}</Text>
              </View>
            )}
          />
        </View>
        <Text style={styles.job}>Job Description</Text>
        <View style={styles.subBox}>
          <Text style={styles.aboutHeading}>What you’ll do</Text>
          <Text style={styles.about}>
            This opening is for 0-6months experience candidates. Should have
            worked on Angular JS/Javascript, MeanStack. Work on agile tools with
            US Clients/Projects. Looking for self motivated individual with zeal
            to learn and grow.
          </Text>
        </View>
        <View style={styles.subBox}>
          <Text style={styles.text}>Industry type</Text>
          <Text style={styles.subText}>Construction Worker</Text>
          <Text style={styles.text}>Functional Area</Text>
          <Text style={styles.subText}>Other</Text>
          <Text style={styles.text}>Employment Type</Text>
          <Text style={styles.subText}>Full Time, Permanent</Text>
          <Text style={styles.text}>Education</Text>
          <Text style={styles.subText}>10th in any Specialization</Text>
          <Text style={styles.subText}>12th in any Specialization</Text>
        </View>
        <Text style={styles.job}>About Company</Text>
        <View style={styles.subBox}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={{
                width: 50,
                height: 50,
                marginLeft: 5,
                marginTop: 15,
              }}
              source={require('../images/images.png')}
            />
            <View>
              <Text style={styles.in2Text}>Ram Construction</Text>
              <Text style={styles.insub2Text}>Washington, United States</Text>
              <Text style={styles.insub2Text}>Above 500</Text>
            </View>
          </View>
          <Text style={styles.about2Heading}>Overview</Text>
          <Text style={styles.about}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 10,
              paddingRight: 20,
              marginBottom: 20,
            }}>
            <Image
              style={{
                width: 92,
                height: 92,
                borderRadius: 15,
              }}
              source={require('../images/image.png')}
            />
            <Image
              style={{
                width: 92,
                height: 92,
                borderRadius: 15,
              }}
              source={require('../images/Reruiting-agent-slice/images.png')}
            />
            <Image
              style={{
                width: 92,
                height: 92,
                borderRadius: 15,
              }}
              source={require('../images/image.png')}
            />
          </View>
        </View>
        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
          <View style={{width: '45%', marginTop: 20}}>
            <EndButton
              title={'APPLY NOW'}
              height={40}
              txtcolor={'#fff'}
              onPress={() => {
                navigation.navigate('MyJobs');
              }}
            />
          </View>
          <View style={{width: '45%', marginTop: 20}}>
            <EndButton
              title={'DECLINE'}
              height={40}
              bgColor={'#DA274D'}
              txtcolor={'#fff'}
              onPress={() => setModalOpen(true)}
            />
          </View>
        </View>
        <BottomView />
        <Modal
          visible={modalOpen}
          transparent={true}
          onRequestClose={() => setModalOpen(false)}>
          <View style={styles.modal_View}>
            <View
              activeOpacity={0.8}
              style={styles.mdtop}
              onPress={() => {
                navigation.navigate('TabNavigator');
              }}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 20,
                }}
                source={require('../images/close.png')}
              />
              <Text style={styles.modaltext}>Decline Job</Text>

              <Text style={styles.modalsubText}>
                are you sure you want to{`\n`}decline this job ?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    navigation.navigate('MyJobs');
                  }}>
                  YES
                </Text>
                <Text style={styles.no}>NO</Text>
              </View>
            </View>
          </View>
        </Modal>
      </ScrollView>
    </View>
  );
};

const JOBSTATUS = () => <View style={{flex: 1, backgroundColor: '#fff'}} />;

const renderScene = SceneMap({
  first: JOBDETAILS,
  second: JOBSTATUS,
});

const JobDetails1 = ({navigation}) => {
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'JOB DETAILS'},
    {key: 'second', title: 'ABOUT COMPANY'},
  ]);
  return (
    <View style={{backgroundColor: '#f8f8f8', flex: 1}}>
      <StatusBarLight />
      <AppHeader
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../images/back.png')}
        title={'Job Details'}
        shareOnClick={() => {}}
        share={require('../images/share.png')}
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            scrollEnabled={false}
            activeColor={'#2574FF'}
            inactiveColor={'#000000'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default JobDetails1;

const styles = StyleSheet.create({
  image: {
    marginTop: height / 3,
    width: 219,
    height: 232,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  style: {backgroundColor: 'white', elevation: 5},
  labelStyle: {
    fontSize: 16,
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    color: '#ACB1C0',
  },
  indicatorStyle: {
    backgroundColor: '#2574FF',
    height: 3,
  },
  subBox: {
    padding: 10,
    marginHorizontal: 15,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 5,
  },
  inText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginLeft: 15,
    marginTop: 15,
  },
  in2Text: {
    fontFamily: 'Muli',
    fontSize: 18,
    fontWeight: '700',
    color: '#2574FF',
    marginLeft: 15,
    marginTop: 10,
  },
  insubText: {
    fontFamily: 'Muli',
    fontSize: 12,
    fontWeight: '600',
    color: '#8F9BB3',
    marginLeft: 15,
    marginTop: 5,
  },
  insub2Text: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '600',
    color: '#8F9BB3',
    marginLeft: 15,
    // marginTop: 5,
  },
  redText: {
    fontFamily: 'Muli',
    fontSize: 12,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 5,
    marginTop: 15,
    marginBottom: 10,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#f5f5f5',
    // marginHorizontal: 20,
    marginTop: 10,
  },
  middleText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '700',
    color: '#8F9BB3',
    marginLeft: 5,
    marginTop: 15,
  },
  boxText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 15,
    marginTop: 13,
  },
  job: {
    fontFamily: 'Muli',
    fontSize: 18,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 20,
    marginTop: 20,
  },
  aboutHeading: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 5,
  },
  about2Heading: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 5,
    marginTop: 20,
  },
  about: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: '600',
    color: '#1E1F20',
    marginLeft: 5,
    marginTop: 5,
    lineHeight: 22,
    textAlign: 'justify',
    marginHorizontal: 20,
  },
  text: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '600',
    color: '#8F9BB3',
    marginLeft: 5,
    marginTop: 5,
  },
  subText: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#1E1F20',
    marginLeft: 5,
    marginTop: 5,
  },
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  modaltext: {
    fontFamily: 'Muli',
    fontWeight: '700',
    fontSize: 18,
    color: '#F72C57',
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 25,
  },
  modalsubText: {
    fontFamily: 'Muli',
    fontWeight: '600',
    fontSize: 15,
    color: '#6F6F7B',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20,
    lineHeight: 22,
  },
  yes: {
    fontFamily: 'Muli',
    fontWeight: '700',
    fontSize: 15,
    color: '#2574FF',
    marginLeft: 'auto',
  },
  no: {
    fontFamily: 'Muli',
    fontWeight: '700',
    fontSize: 15,
    color: '#F72C57',
    marginHorizontal: 20,
    marginBottom: 20,
  },
});
