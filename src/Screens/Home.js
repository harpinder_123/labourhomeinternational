import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarLight} from '../Custom/CustomStatusBar';
import {BottomView, EndButton, HeaderLight} from '../Custom/CustomView';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import Header from '../home/Header';
const {height} = Dimensions.get('window');

const Home = ({navigation}) => {
  const [data, setData] = useState([
    {
      id: '1',
      title: 'Ram Construction',
      subTitle: 'Washington, United States',
      source: require('../images/images.png'),
    },
    {
      id: '2',
      title: 'Ram Construction',
      subTitle: 'Washington, United States',
      source: require('../images/Reruiting-agent-slice/images.png'),
    },
    {
      id: '3',
      title: 'Ram Construction',
      subTitle: 'Washington, United States',
      source: require('../images/images.png'),
    },
  ]);
  return (
    <SafeAreaView style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarLight />
      <Header />
      <View style={{flexDirection: 'row'}}>
        <View style={{marginTop: 85, marginLeft: 30}}>
          <AnimatedCircularProgress
            size={60}
            width={5}
            fill={70}
            rotation={10}
            tintColor="#fff"
            backgroundColor="#ffffff4d">
            {fill => (
              <TouchableOpacity onPress={() => changehandler()}>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#fff',
                    fontWeight: '700',
                    // marginHorizontal: 20,
                  }}>
                  {'85%'}
                </Text>
              </TouchableOpacity>
            )}
          </AnimatedCircularProgress>
        </View>
        <View>
          <Text style={styles.name}>Brian Jonathan</Text>
          <Text style={styles.subName}>Labour Worker</Text>
          <Text style={styles.updated}>Last Updated 2 days ago</Text>
        </View>
        <Image
          style={styles.headerImage}
          source={require('../images/dp.png')}
        />
      </View>
      <View style={styles.box}>
        <Text style={styles.boxText}>Search For?</Text>
        <View style={{flexDirection: 'row'}}>
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'Job Title, Company etc'}
          />
          <Image
            style={{
              width: 13,
              height: 13,
              marginTop: 20,
              marginLeft: 'auto',
              marginHorizontal: 20,
            }}
            source={require('../images/search.png')}
          />
        </View>
        <View style={styles.underLine} />
        <Text style={styles.boxText}>Where</Text>
        <View style={{flexDirection: 'row'}}>
          <TextInput
            // value={state.name}
            //   onChangeText={name => setState({...state, name})}
            style={styles.textInput}
            placeholder={'Enter Location'}
          />
          <Image
            style={{
              width: 12,
              height: 16,
              marginTop: 10,
              marginLeft: 'auto',
              marginHorizontal: 20,
            }}
            source={require('../images/location.png')}
          />
        </View>
      </View>
      <ScrollView decelerationRate={0.5}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.scheduleText}>Schedule Interview</Text>
          <Text style={styles.viewText}>View All</Text>
        </View>
        <View>
          <ScrollView decelerationRate={0.5} horizontal>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <SafeAreaView style={styles.subBox}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        marginLeft: 10,
                        marginTop: 15,
                      }}
                      source={item.source}
                    />
                    <View>
                      <Text
                        onPress={() => {
                          navigation.navigate('JobDetails');
                        }}
                        style={styles.inText}>
                        {item.title}
                      </Text>
                      <Text style={styles.insubText}>{item.subTitle}</Text>
                    </View>
                    <Image
                      style={{
                        width: 13,
                        height: 13,
                        marginLeft: 'auto',
                        marginHorizontal: 15,
                        marginTop: 15,
                      }}
                      source={require('../images/star.png')}
                    />
                  </View>
                  <View style={styles.Line} />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.redText}>26 Dec 2021, 1:30 PM</Text>
                    <View
                      style={{
                        width: '40%',
                        marginTop: 10,
                        marginLeft: 'auto',
                      }}>
                      <EndButton title={'Interview Call'} onPress={() => {}} />
                    </View>
                  </View>
                </SafeAreaView>
              )}
            />
          </ScrollView>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Text style={styles.scheduleText}>Recommended Jobs</Text>
          <Text
            style={styles.viewText}
            onPress={() => {
              navigation.navigate('RecommendedJobs');
            }}>
            View All
          </Text>
        </View>
        <View>
          <ScrollView decelerationRate={0.5} horizontal>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <SafeAreaView style={styles.subBox}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        marginLeft: 10,
                        marginTop: 15,
                      }}
                      source={item.source}
                    />
                    <View>
                      <Text style={styles.inText}>{item.title}</Text>
                      <Text style={styles.insubText}>{item.subTitle}</Text>
                    </View>
                    <Image
                      style={{
                        width: 13,
                        height: 13,
                        marginLeft: 'auto',
                        marginHorizontal: 15,
                        marginTop: 15,
                      }}
                      source={require('../images/star.png')}
                    />
                  </View>
                  <View style={styles.Line} />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.redText}>26 Dec 2021, 1:30 PM</Text>
                    <View
                      style={{
                        width: '40%',
                        marginTop: 10,
                        marginLeft: 'auto',
                      }}>
                      <EndButton title={'Interview Call'} onPress={() => {}} />
                    </View>
                  </View>
                </SafeAreaView>
              )}
            />
          </ScrollView>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Text style={styles.scheduleText}>New Post Jobs</Text>
          <Text style={styles.viewText}>View All</Text>
        </View>
        <View>
          <ScrollView decelerationRate={0.5} horizontal>
            <FlatList
              numColumns={3}
              keyExtractor={item => item.id}
              data={data}
              renderItem={({item, index}) => (
                <SafeAreaView style={styles.subBox}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        marginLeft: 10,
                        marginTop: 15,
                      }}
                      source={item.source}
                    />
                    <View>
                      <Text style={styles.inText}>{item.title}</Text>
                      <Text style={styles.insubText}>{item.subTitle}</Text>
                    </View>
                    <Image
                      style={{
                        width: 13,
                        height: 13,
                        marginLeft: 'auto',
                        marginHorizontal: 15,
                        marginTop: 15,
                      }}
                      source={require('../images/star.png')}
                    />
                  </View>
                  <View style={styles.Line} />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.redText}>26 Dec 2021, 1:30 PM</Text>
                    <View
                      style={{
                        width: '40%',
                        marginTop: 10,
                        marginLeft: 'auto',
                      }}>
                      <EndButton title={'Interview Call'} onPress={() => {}} />
                    </View>
                  </View>
                </SafeAreaView>
              )}
            />
          </ScrollView>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  roundCircle: {
    borderWidth: 5,
    width: 60,
    height: 60,
    borderRadius: 50,
    borderColor: '#fff',
    borderTopColor: 'grey',
    marginTop: '20%',
    marginLeft: 20,
  },
  roundText: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 12,
  },
  name: {
    fontFamily: 'Muli',
    fontSize: 16,
    fontWeight: '700',
    color: '#fff',
    marginTop: 80,
    marginLeft: 15,
  },
  subName: {
    fontFamily: 'Muli',
    fontSize: 13,
    fontWeight: '700',
    color: '#fff',
    marginTop: 5,
    marginLeft: 15,
  },
  updated: {
    fontFamily: 'Muli',
    fontSize: 12,
    fontWeight: '600',
    color: 'lightgrey',
    marginTop: 7,
    marginLeft: 15,
  },
  headerImage: {
    width: 65,
    height: 63,
    borderRadius: 10,
    marginTop: 85,
    marginLeft: 'auto',
    marginHorizontal: 30,
  },
  box: {
    padding: 5,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    marginHorizontal: 20,
    marginTop: 20,
  },
  boxText: {
    fontFamily: 'Muli',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginLeft: 10,
    marginTop: 10,
  },
  textInput: {
    marginLeft: 5,
    fontSize: 14,
    fontFamily: 'Muli-SemiBold',
    fontWeight: '600',
    backgroundColor: '#fff',
    borderColor: 'lightgrey',
    marginHorizontal: 15,
    width: '55%',
  },
  underLine: {
    height: 1,
    borderRadius: 5,
    backgroundColor: 'lightgrey',
    marginHorizontal: 10,
    marginTop: -5,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#f5f5f5',
    // marginHorizontal: 20,
    marginTop: 10,
  },
  scheduleText: {
    fontFamily: 'Muli',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginLeft: 20,
    marginTop: 20,
  },
  viewText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#2473FD',
    marginLeft: 'auto',
    marginTop: 20,
    marginHorizontal: 30,
  },
  subBox: {
    width: 275,
    height: 120,
    backgroundColor: '#fff',
    elevation: 5,
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 10,
  },
  inText: {
    fontFamily: 'Muli',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginLeft: 15,
    marginTop: 15,
  },
  insubText: {
    fontFamily: 'Muli',
    fontSize: 12,
    fontWeight: '600',
    color: '#8F9BB3',
    marginLeft: 15,
    marginTop: 5,
  },
  redText: {
    fontFamily: 'Muli',
    fontSize: 11,
    fontWeight: '600',
    color: '#FF0000',
    marginLeft: 15,
    marginTop: 15,
  },
});
