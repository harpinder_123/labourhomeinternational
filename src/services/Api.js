import AsyncStorage from '@react-native-async-storage/async-storage';
import { request, requestGet, requestMultipart } from './ApiSauce';
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const Api = {
    // reuriting_agentrRegister
    login: json => request('login', json),
    verfiyOtp: json => request('verfiyOtp', json),
    reuritingAgentrRegister: json => request('reuriting_agentrRegister', json),
    projectExporterRegister: json => request('project_exporterRegister', json),
    foreignRecuritingRegister: json => request('foreign_recuritingRegister', json),
    custmerRegisterStep1: json => request('custmerRegister_step1', json),
    custmerRegisterStep2: json => request('custmerRegister_step2', json),
    custmerRegisterStep3: json => request('custmerRegister_step3', json),
    custmerRegisterStep4: json => request('custmerRegister_step4', json),
    state: json => request('state', json),
    city: json => request('city', json),
    sub_category: json => request('sub_category', json),
    fetchSettings: json => request('fetch_settings', json),
    getUserProfile: json => request('get_user_profile', json),
    countrie: () => requestGet('countrie'),
    category: () => requestGet('category'),
    faq: () => requestGet('faq'),
    // fetchSettings: () => requestGet('fetch_settings'),
    // custmerRegisterStep3: formData => requestMultipart('custmerRegisterStep3', formData),

};
const LocalStorage = {
    setToken: token => AsyncStorage.setItem('authToken', token),
    getToken: () => AsyncStorage.getItem('authToken'),
    setFcmToken: fcmToken => AsyncStorage.setItem('fcmToken', fcmToken),
    getFcmToken: () => AsyncStorage.getItem('fcmToken'),
    setUserDetail: user_detail => AsyncStorage.setItem('userdata', user_detail),
    getUserDetail: () => AsyncStorage.getItem('userdata'),
    setLanguage: language => AsyncStorage.setItem('language', language),
    getLanguage: () => AsyncStorage.getItem('language'),
    setFirstTimeOpen: () => AsyncStorage.setItem('firstTimeOpen', 'false'),
    getFirstTimeOpen: () => AsyncStorage.getItem('firstTimeOpen'),
    clear: AsyncStorage.clear,
};

// const onSearchEvent = new Subject().pipe(debounceTime(500));

export { width, height, Api, LocalStorage };
